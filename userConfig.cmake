#############################################################
##  Set the name of your project
#############################################################

set(PROJECT_NAME hl_BaseProject)


#############################################################
##  Set your applications
#############################################################

## Add applications that do not depend on vtk and gmsh:
#list(APPEND APPS EmptyApp)
# list(APPEND APPS MyApp_without_VTK_and_GMSH)

## Add applications that depend both on vtk and gmsh:
# list(APPEND APPS_VTK_GMSH MyApp_with_VTK_and_GMSH)

## Add applications that depend on vtk:
# list(APPEND APPS_VTK MyApp_with_VTK)

## Add applications that depend on gmsh:
# list(APPEND APPS_GMSH MyApp_with_GMSH)
